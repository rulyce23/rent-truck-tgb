-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Agu 2020 pada 17.17
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_rentaltruck`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `angkut_barang`
--

CREATE TABLE `angkut_barang` (
  `id` varchar(36) NOT NULL,
  `manufacture_id` varchar(37) NOT NULL,
  `name` varchar(70) NOT NULL,
  `detail` varchar(100) NOT NULL,
  `weight` varchar(60) NOT NULL,
  `weight_stock` int(11) NOT NULL,
  `jumlah_shipment` int(11) NOT NULL,
  `tanggal_produksi` datetime NOT NULL,
  `price` int(10) UNSIGNED DEFAULT NULL,
  `car_id` varchar(37) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `angkut_barang`
--

INSERT INTO `angkut_barang` (`id`, `manufacture_id`, `name`, `detail`, `weight`, `weight_stock`, `jumlah_shipment`, `tanggal_produksi`, `price`, `car_id`, `deleted_at`, `updated_at`, `created_at`) VALUES
('2249378e-32e2-4300-b3f7-0de6e729bdbb', '86c10da8-c097-4157-85f1-c8ac27d5445e', 'Asus Vivo Book', 'Notebook Vivo Book Perlu Diangkut & Diantarkan', 'Gram', 230, 12, '2020-07-26 00:00:00', 328000, NULL, NULL, '2020-08-03 06:36:47', '2020-08-03 06:36:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_images`
--

CREATE TABLE `barang_images` (
  `id` varchar(36) NOT NULL,
  `barang_id` varchar(36) NOT NULL,
  `image` varchar(191) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang_images`
--

INSERT INTO `barang_images` (`id`, `barang_id`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
('ba8ee3ea-bccc-454e-a384-e17d30bdb6e4', '25d3ea69-da22-426b-8394-03f21eeb25d7', 'storage/image/goods/7cd99a5d-061c-4013-82ac-f84bc6bd1b7b.jpeg', NULL, '2020-08-03 06:14:02', '2020-08-03 06:14:02'),
('54ac788f-6b6d-4a3f-8bb8-7a701844ebfd', '6a5af59d-6d69-4aa1-a13c-a26f4c46dd2b', 'storage/image/goods/734269fe-03ee-467e-b6af-35591d957f6d.jpeg', NULL, '2020-08-03 06:15:57', '2020-08-03 06:15:57'),
('677f64e5-ccab-401a-b2e2-7459b406391f', '2249378e-32e2-4300-b3f7-0de6e729bdbb', 'storage/image/goods/86c9797d-88c3-40a0-a85f-a525534a7fd9.jpeg', NULL, '2020-08-03 06:36:47', '2020-08-03 06:36:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cars`
--

CREATE TABLE `cars` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacture_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `license_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `capacity` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `price` int(10) UNSIGNED DEFAULT NULL,
  `penalty` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cars`
--

INSERT INTO `cars` (`id`, `manufacture_id`, `name`, `license_number`, `capacity`, `color`, `year`, `status`, `price`, `penalty`, `deleted_at`, `created_at`, `updated_at`) VALUES
('496a13f7-fc10-4bff-9c6a-417593c660a6', '47eeb866-17dd-4a88-a2e3-63288f3ff707', 'Truck Fuso FXG-L420', 'D 1384 RG', 'Big', 'Kuning', '2013', 'terpakai', 78000, 10000, NULL, '2020-08-03 05:57:32', '2020-08-03 07:34:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `car_images`
--

CREATE TABLE `car_images` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `car_images`
--

INSERT INTO `car_images` (`id`, `car_id`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
('a943ef1c-74a9-42d8-b173-70111806ae2c', '44c67577-8e8b-4973-95b1-405cdc3f8dbb', 'storage/image/car/b6f8b52c-2470-461d-9ee4-4e54b512ecab.png', NULL, '2020-08-03 05:36:48', '2020-08-03 05:36:48'),
('5f097092-9ba6-4492-8cfa-e356e001fd27', '496a13f7-fc10-4bff-9c6a-417593c660a6', 'storage/image/car/95b0e25f-5f54-4be9-8ae3-b82bf2dc65ae.png', NULL, '2020-08-03 05:57:32', '2020-08-03 05:57:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `nik` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `sex` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`id`, `name`, `slug`, `nik`, `sex`, `address`, `phone_number`, `email`, `deleted_at`, `created_at`, `updated_at`) VALUES
('4d984f3a-85df-45c2-a291-ac36f39d47c8', 'Ruly Rizki Perdana', 'Ruly Rizki Perdana', '3204082307960001', 'laki-laki', 'Komp GBA 3 Blok B1 No.10 Kec.Bojongsoang RT 001 RW 012', '089663209366', 'rulyce23@gmail.com', '2020-08-03 04:28:46', '2020-08-03 04:28:28', '2020-08-03 04:28:46'),
('d1f9b764-748e-4153-87fd-39b6d3beb70d', 'Ruly Rizki Perdana', 'Ruly Rizki Perdana', '3204082307960001', 'laki-laki', 'Komp GBA 3 Blok B1 No.10 Kec.Bojongsoang RT 001 RW 012', '089663209366', 'rulyce23@gmail.com', NULL, '2020-08-03 05:37:27', '2020-08-03 05:37:27'),
('cd617b13-9c0b-4749-b535-62c32d491b27', 'Ruly Rizki Perdana', 'Ruly Rizki Perdana', '3204082307960001', 'laki-laki', 'Komp GBA 3 Blok B1 No.10 Kec.Bojongsoang RT 001 RW 012', '089663209366', 'rulyce23@gmail.com', '2020-08-03 07:30:30', '2020-08-03 07:03:35', '2020-08-03 07:30:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `manufacture2`
--

CREATE TABLE `manufacture2` (
  `id` varchar(36) NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `manufacture2`
--

INSERT INTO `manufacture2` (`id`, `name`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
('86c10da8-c097-4157-85f1-c8ac27d5445e', 'Laptop', 'Laptop', NULL, '2020-08-03 04:41:37', '2020-08-03 04:41:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `manufactures`
--

CREATE TABLE `manufactures` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `manufactures`
--

INSERT INTO `manufactures` (`id`, `name`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
('47eeb866-17dd-4a88-a2e3-63288f3ff707', 'Mitsubishi Fuso', 'Mitsubishi Fuso', NULL, '2020-08-03 05:35:52', '2020-08-03 05:35:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_05_110508_create_roles_table', 1),
(4, '2019_04_27_083809_create_settings_table', 1),
(5, '2019_05_14_082540_create_customers_table', 1),
(6, '2019_05_14_082600_create_transactions_table', 1),
(7, '2019_05_17_143152_create_manufactures_table', 1),
(8, '2019_05_17_143557_create_car_images_table', 1),
(9, '2019_05_17_143613_create_cars_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
('b27919a9-cb7a-4e81-8b14-50f020f05b31', 'Admin', 'Admin', NULL, '2020-08-03 07:20:49', '2020-08-03 07:20:49'),
('7b4c54cb-44d1-4e87-8f74-3736ac948344', 'Customer', 'Customer', NULL, '2020-08-03 07:20:57', '2020-08-03 07:20:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `name`, `slug`, `description`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
('1', 'CV Jaya Abadi Truck Delivery N Shipment Rent', 'nama-toko', 'CV Jaya Abadi', 'textarea', NULL, NULL, '2020-08-03 07:11:47'),
('2', 'Jl. Gedebage No. 83 Bandung', 'alamat', 'alamat perusahaan rental', 'textarea', NULL, NULL, NULL),
('3', '081475199123', 'nomer-telepon', 'telepon n contact person untuk rental kendaraan truck dalam angkut barang-barang penting', 'text', NULL, NULL, NULL),
('4', 'jayaabadirent@gmail.com', 'email', 'email toko perusahaan', 'text', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

CREATE TABLE `transactions` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barang_id` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `rent_date` datetime DEFAULT NULL,
  `back_date` datetime DEFAULT NULL,
  `return_date` datetime DEFAULT NULL,
  `price` int(10) UNSIGNED DEFAULT NULL,
  `price_barang` int(11) NOT NULL,
  `amount` int(10) UNSIGNED DEFAULT NULL,
  `penalty` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transactions`
--

INSERT INTO `transactions` (`id`, `customer_id`, `car_id`, `barang_id`, `invoice_no`, `rent_date`, `back_date`, `return_date`, `price`, `price_barang`, `amount`, `penalty`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
('2c79f55a-95b6-475a-a792-c5ad13441710', 'cd617b13-9c0b-4749-b535-62c32d491b27', '496a13f7-fc10-4bff-9c6a-417593c660a6', '2249378e-32e2-4300-b3f7-0de6e729bdbb', 'TRX-030820-00001', '2020-08-03 00:00:00', '2020-08-07 00:00:00', '2020-08-05 00:00:00', 78000, 328000, 660000, 20000, 'selesai', NULL, '2020-08-03 07:03:35', '2020-08-03 07:04:49'),
('510c44db-693b-45f3-8f52-009e48b15937', 'd1f9b764-748e-4153-87fd-39b6d3beb70d', '496a13f7-fc10-4bff-9c6a-417593c660a6', '2249378e-32e2-4300-b3f7-0de6e729bdbb', 'TRX-030820-00002', '2020-08-16 00:00:00', '2020-08-18 00:00:00', NULL, 78000, 328000, 484000, NULL, 'proses', NULL, '2020-08-03 07:34:22', '2020-08-03 07:34:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
('9657df72-8eef-4e37-87ce-8f316c640781', '7b4c54cb-44d1-4e87-8f74-3736ac948344', 'Adibarri Rulyanto', 'abi16', 'adibarrirulyanto@gmail.com', NULL, '$2y$10$DAuzrT/slbp/CCH8dp2XmOa08EOs5xEtfdSxi8AJ81bIainz5yiLG', NULL, NULL, '2020-08-03 07:22:42', '2020-08-03 07:22:42'),
('1', '1', 'ruly', 'ruly', 'rulyce23@gmail.com', '2020-08-02 17:00:00', '$2y$10$9fh8.UvrIe7zPKWkmJo7jeZgEdMLJV.tT4.eKmSAK6XojnT2myyR.', 'JEhwjddns3MQadCkgPAA0e6YA3iHNeMUFmChGPGLcTsdLWBj2kHQv7apz4sy', NULL, '2020-08-02 17:00:00', '2020-08-03 04:29:19');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `angkut_barang`
--
ALTER TABLE `angkut_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
