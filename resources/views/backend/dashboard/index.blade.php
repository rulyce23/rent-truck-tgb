@extends('backend.layouts')
@section('title','Dashboard')
@section('content')
<div class="col-xl-3 col-md-6 mb-4">
    <a href="{{route('car.index')}}" style="text-decoration:none;">
    <div class="card border-left-primary shadow-sm h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Truk Angkut Tersedia</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$car->where('status','tersedia')->get()->count()}} / {{$car->get()->count()}}</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-truck fa-2x text-primary-300"></i>
                </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-xl-3 col-md-6 mb-4">
    <a href="{{route('customer.index')}}" style="text-decoration:none;">
    <div class="card border-left-success shadow-sm h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Customer</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$customer->get()->count()}}</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-user fa-2x text-success-300"></i>
                </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-xl-3 col-md-6 mb-4">
    <a href="{{route('transaction.history')}}" style="text-decoration:none;">
    <div class="card border-left-info shadow-sm h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Transaksi</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$transaction->where('status','selesai')->get()->count()}}</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-database fa-2x text-info-300"></i>
                </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-xl-3 col-md-6 mb-4">
    <a href="{{route('transaction.index')}}" style="text-decoration:none;">
    <div class="card border-left-danger shadow-sm h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Transaksi Aktif</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$transaction->where('status','proses')->get()->count()}}</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-book fa-2x text-danger-300"></i>
                </div>
            </div>
        </div>
    </div>
    </a>
</div>
<div class="col-lg-6">
    <div class="card shadow-sm mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Transaksi Tahun {{date('Y')}}</h6>
        </div>
        <div class="card-body-primary">
                {!! $chartjs->render() !!}
        </div>
    </div>
	
	
</div>
<div class="col-lg-6">

<style>
.map-responsive{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
}
.map-responsive iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}
</style>
	<div class="map-responsive">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15841.06357956358!2d107.65704779999999!3d-6.977921299999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sid!4v1596466492766!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	   </div>
	</div>

@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" type="text/javascript"></script>
@endpush
