<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-left justify-content-left" href="#">
        <div class="sidebar-brand-text">{{App\Setting::where('slug','nama-toko')->get()->first()->description}}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">

    </div>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{active('dashboard')}}">
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="fas fa-fw fa-home"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item {{active('customer.index')}}">
        <a class="nav-link" href="{{route('customer.index')}}">
            <i class="fas fa-fw fa-user"></i>
            <span>Customer</span>
        </a>
    </li>
  
    <li class="nav-item">
        <a class="nav-link {{is_active('car.index') ? '':is_active('manufacture.index') ? '':'collapsed'}}" href="#" data-toggle="collapse" data-target="#car" aria-expanded="true" aria-controls="car">
            <i class="fas fa-fw fa-truck"></i>
            <span>Data Transportasi</span>
        </a>
        <div id="car" class="collapse {{is_active('car.index') || is_active('manufacture.index')  ? 'show':''}}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{active('car.index')}}" href="{{route('car.index')}}">Mobil Truck</a>
            <a class="collapse-item {{active('manufacture.index')}}" href="{{route('manufacture.index')}}">Merk</a>
            </div>
        </div>
    </li>
    {{-- <li class="nav-item {{active('car.index')}}">
        <a class="nav-link" href="{{route('car.index')}}">
           
            <span>Mobil</span>
        </a>
    </li>
    <li class="nav-item {{active('manufacture.index')}}">
        <a class="nav-link" href="{{route('manufacture.index')}}">
          
            <span>Merk</span>
        </a>
    </li> --}}
	
	
	  
    <li class="nav-item">
        <a class="nav-link {{is_active('goods.index') ? '':is_active('manufactures.index') ? '':'collapsed'}}" href="#" data-toggle="collapse" data-target="#goods" aria-expanded="true" aria-controls="goods">
            <i class="fas fa-fw fa-archive"></i>
            <span>Data Barang</span>
        </a>
        <div id="goods" class="collapse {{is_active('goods.index') || is_active('manufactures.index')  ? 'show':''}}" aria-labelledby="headingTwos" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{active('goods.index')}}" href="{{route('goods.index')}}">Angkut Barang</a>
            <a class="collapse-item {{active('manufactures.index')}}" href="{{route('manufactures.index')}}">Merk Produk Barang</a>
            </div>
        </div>
    </li>
    {{-- <li class="nav-item {{active('goods.index')}}">
        <a class="nav-link" href="{{route('goods.index')}}">
            <i class="fas fa-fw fa-archive"></i>
            <span>Angkut Barang</span>
        </a>
    </li>
    <li class="nav-item {{active('manufactures.index')}}">
        <a class="nav-link" href="{{route('manufactures.index')}}">
            <i class="fas fa-fw fa-file"></i>
            <span>Merk Produk Barang</span>
        </a>
    </li> --}}
	
	
	

    <li class="nav-item">
        <a class="nav-link {{is_active('transaction.*') ? '':'collapsed'}}" href="#" data-toggle="collapse" data-target="#transaksi" aria-expanded="true" aria-controls="transaksi">
            <i class="fas fa-fw fa-book"></i>
            <span>Transaksi</span>
        </a>
        <div id="transaksi" class="collapse {{is_active('transaction.*')  ? 'show':''}}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{active('transaction.create')}}" href="{{route('transaction.create')}}">Transaksi</a>
            <a class="collapse-item {{active('transaction.index')}}" href="{{route('transaction.index')}}">List Transaksi</a>
            <a class="collapse-item {{active('transaction.history')}}" href="{{route('transaction.history')}}">Riwayat Transaksi</a>
            </div>
        </div>
    </li>
    {{-- <li class="nav-item {{active('transaction.create')}}">
        <a class="nav-link" href="{{route('transaction.create')}}">
            <i class="fas fa-fw fa-book"></i>
            <span>Transaksi</span>
        </a>
    </li>
    <li class="nav-item {{active('transaction.index')}}">
        <a class="nav-link" href="{{route('transaction.index')}}">
            <i class="fas fa-fw fa-book"></i>
            <span>List Transaksi</span>
        </a>
    </li>
    <li class="nav-item {{active('transaction.history')}}">
        <a class="nav-link" href="{{route('transaction.history')}}">
            <i class="fas fa-fw fa-book"></i>
            <span>Riwayat Transaksi</span>
        </a>
    </li> --}}

    <li class="nav-item">
        <a class="nav-link {{is_active('user.index') || is_active('role.index') ? '':'collapsed'}}" href="#" data-toggle="collapse" data-target="#user" aria-expanded="true" aria-controls="user">
            <i class="fas fa-fw fa-user"></i>
            <span>Manajemen Pengguna</span>
        </a>
        <div id="user" class="collapse {{is_active('user.index') || is_active('role.index')  ? 'show':''}}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item {{active('user.index')}}" href="{{route('user.index')}}">Pengguna</a>
            <a class="collapse-item {{active('role.index')}}" href="{{route('role.index')}}">Hak Akses</a>
            </div>
        </div>
    </li>

</ul>
<!-- End of Sidebar -->
