@extends('backend.layouts')
@section('title','Barang')
@section('content')
<div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
        </div>
        <div class="card-body">
            <table class="table table-sm table-bordered table-striped" id="goods-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Barang</th>
                        <th>Manufaktur Barang</th>
                        <th>Berat Satuan</th>
                        <th>Berat Masa </th>
                        <th>Jumlah Angkut</th>
                        <th>Harga Angkut</th>
                        <th>ID Truk Angkut </th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@include('backend.goods.modal-show')
@endsection
@push('scripts')
<script src="{{ asset('backend/js/sweet-alert.min.js') }}"></script>
<script>
$(document).ready(function () {

    $.fn.dataTable.ext.errMode = 'throw';
    var $table = $('#goods-table').DataTable({
         processing: true,
         serverSide: true,
         responsive: true,
         language: {
            paginate: {
                next: '<i class="fa fa-angle-right"></i>',
                previous: '<i class="fa fa-angle-left"></i>'
            },
            processing: 'Loading . . .',
            emptyTable: 'Tidak Ada Data',
            zeroRecords: 'Tidak Ada Data'
         },
         stateSave: true,
         dom: '<"toolbar">rtp',
         ajax: '{!! route('goods.source') !!}',
         columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',width:"2%", orderable : false},
            // {data: 'code', name: 'code',width:"5%", orderable : false},
            {data: 'name', name: 'name',width:"5%", orderable : false},
            {data: 'manufactures', name: 'manufactures',width:"5%", orderable : false},
            {data: 'weight', name: 'weight',width:"5%", orderable : false},
            {data: 'weight_stock', name: 'weight_stock',width:"5%", orderable : false},
			{data: 'jumlah_shipment', name: 'jumlah_shipment',width:"5%", orderable : false},
			{data: 'price', name: 'price',width:"5%", orderable : false},
			{data: 'car_id', name: 'car_id',width:"5%", orderable : false},
            {data: 'action', name: 'action',width:"5%", orderable : false}
         ]
     });

      $('#goods-table_wrapper > div.toolbar').html('<div class="row">' +
                '<div class="col-lg-10">'+
                    '<div class="input-group mb-3"> ' +
                        '<input type="text" class="form-control form-control-sm border-0 bg-light" id="search-box" placeholder="Masukkan Kata Kunci"> ' +
                        '<div class="input-group-append">' +
                        '<span class="btn btn-primary btn-sm"><i class="fas fa-search"></i></span>' +
                        '</div>' +
                    '</div>' +
                '</div>'+
                '<div class="col-lg-2">'+
                    '<a href="{{ route("goods.create") }}" class="btn btn-primary btn-sm shadow-sm float-right" data-toggle="tooltip" title="Tambah Data"><i class="fas fa-plus"></i></a>'+
                '</div>' +
                '</div>');

     $(document).on('keypress','#search-box',function (e) {
            if(e.which == '13'){
                $table.search($(this).val()).draw();
            }
        //  e.preventDefault();
     });


    $('#goods-table').on('click','a.delete-data',function(e) {
        e.preventDefault();
        var delete_link = $(this).attr('href');
        swal({
            title: "Hapus Data ini?",
            text: "",
            icon: "error",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Data anda telah terhapus");
                    window.location.replace(delete_link);
                } else {
                    swal("Data anda masih aman dan belum terhapus");
                }
            });
    });

    $('body').tooltip({selector: '[data-toggle="tooltip"]'});

    $('#show').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var name = button.data('name'); // Extract info from data-* attributes
        var detail = button.data('detail'); // Extract info from data-* attributes
        var weight = button.data('weight'); // Extract info from data-* attributes
        var weight_stock = button.data('weight_stock'); // Extract info from data-* attributes
        var jumlah_shipment = button.data('jumlah_shipment'); // Extract info from data-* attributes
        var price = button.data('price'); // Extract info from data-* attributes
        var car_id = button.data('car_id'); // Extract info from data-* attributes
       var url = "{!! route('manufactures.find',':id') !!}";
        url = url.replace(':id',button.data('manufacture_id'));
        $.getJSON(url, function(data){
            var manufactures = data.name;
            modal.find('input[name="manufactures"]').val(manufactures);
        });

        var url_image = "{!! route('goods.getImage',':id_goods') !!}";
        url_image = url_image.replace(':id_goods',button.data('id'));
        $.getJSON(url_image, function(data){
            $.each(data, function(index,value){
                if(index == 0){
                    var active = 'active';
                }
                var image = "{!! asset('image') !!}";
                image = image.replace('image',value.image);
                $('.gambar').append(
                    '<div class="carousel-item '+active+'">'+
                        '<img src="'+image+'" alt="{{asset("backend/img/logo.png")}}" class="d-block w-100">'+
                '</div>');
            });
        });

        var modal = $(this)

        modal.find('input[name="name"]').val(name);
        modal.find('input[name="detail"]').val(detail);
        modal.find('input[name="weight"]').val(weight);
        modal.find('input[name="weight_stock"]').val(weight_stock);
        modal.find('input[name="jumlah_shipment"]').val(jumlah_shipment);
        modal.find('input[name="price"]').val(price);
        modal.find('input[name="car_id"]').val(car_id);

    });

    $('#show').on('hidden.bs.modal', function (event){
        $('.gambar').empty();
    });
});
</script>
@endpush
