@extends('backend.layouts')
@section('title','Tambah Data')
@section('content')
<div class="col-lg-12">
    <div class="card mb-4">
        <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">@yield('title')</h6>
        </div>
        <div class="card-body">
            <form action="{{route('goods.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Gambar</label>
                          <input type="file" name="image[]" id="fileinput" class="form-control border-dark-50" required="" multiple>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Nama</label>
                          <input type="text" name="name" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                          <label>Merk</label>
                            <select name="manufacture_id" class="form-control select2">
                                @foreach (App\Manufactures::orderBy('name','asc')->get() as $row)
                                <option value="{{$row->id}}">{{title_case($row->name)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Deskripsi Barang</label>
                          <input type="text" name="detail" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                          <label>Berat Satuan</label>
                          <input type="text" name="weight" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
				 <div class="row">

					      <div class="col">
                        <div class="form-group">
                          <label>Berat Masa</label>
                          <input type="text" name="weight_stock" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
             
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Jumlah Angkut</label>
                          <input type="text" name="jumlah_shipment" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
					<div class="col">
                        <div class="form-group">
                          <label>Harga Angkut</label>
                          <input type="text" name="price" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
                </div>
				
				      <div class="row">
                    <div class="col">
                        <div class="form-group">
                          <label>Tanggal Barang Diproduksi / Dikeluarkan</label>
                          <input type="date" name="tanggal_produksi" id="" class="form-control border-dark-50" required="">
                        </div>
                    </div>
					<div class="col">
                        <div class="form-group">
                          <label>Truk Angkutan</label>
                      <select option="car_id" name="car_id" id="car_id" class="form-control">
					  @foreach($items as $item)
						<option value="{{ $item->car_id }}">{{ $item->name }}</option>
					  @endforeach
					</select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-gorup">
                            <button type="submit" class="btn btn-primary  shadow-sm">Simpan</button>
                            <a class="btn btn-light shadow-sm" href="{{route('goods.index')}}">Batal</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    $('.select2').select2({
            dropdownParent: $('body'),
            theme: 'bootstrap'
    });

    $('#fileinput').fileinput({
        uploadUrl:'#',
          browseClass: "btn btn-primary btn-block",
          fileActionSettings:{
            showZoom:false,
            showUpload:false,
            removeClass: "btn btn-danger",
            removeIcon: "<i class='fa fa-trash'></i>"
          },
          showCaption: false,
          showRemove: false,
          showUpload: false,
          showCancel: false,
          dropZoneEnabled: false,
          allowedFileExtensions: ['jpg', 'png','jpeg'],
    });
</script>

@endpush
