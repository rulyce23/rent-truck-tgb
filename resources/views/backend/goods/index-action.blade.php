<span data-toggle="modal" data-target="#show" data-name="{{$name}}"
    data-id="{{$id}}"
    data-manufacture_id="{{$manufacture_id}}"
    data-weight="{{$weight}}"
    data-weight_stock="{{$weight_stock}}"
    data-detail="{{$detail}}"
    data-jumlah_shipment="{{$jumlah_shipment}}"
    data-tanggal_produksi="{{$tanggal_produksi}}"
    data-car_id="{{$car_id}}"
    data-price="{{number_format($price,0,'.','.')}}"
    >
    <a href="#"
        class="btn btn-info btn-sm shadow-sm"
        data-toggle="tooltip"
        data-placement="top"
        title="Detail">
        <i class="fa fa-search"></i>
    </a>
</span>
<a href="{{route('goods.edit',$id)}}"
    class="btn btn-success btn-sm shadow-sm"
    data-toggle="tooltip"
    data-placement="top"
    title="Edit">
    <i class="fa fa-pen"></i>
</a>
<a href="{{route('goods.destroy',$id)}}"
    class="btn btn-danger btn-sm shadow-sm delete-data"
    data-toggle="tooltip"
    data-placement="top"
    title="Delete">
    <i class="fa fa-times"></i>
</a>
