<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class GoodsImage extends Model
{
    use SoftDeletes;
    use Uuids;

    protected $table = 'barang_images';
    protected $dates = ['deleted_at'];
    protected $fillable = ['barang_id','image'];
    public $incrementing = false;

    public function goods()
    {
        return $this->belongsTo('App\Goods');
    }

}
