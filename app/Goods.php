<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Goods extends Model
{
    use SoftDeletes;
    use Uuids;

    protected $table = 'angkut_barang';
    protected $dates = ['deleted_at'];
    protected $fillable = ['name','manufacture_id','detail','weight','weight_stock','jumlah_shipment','price','tanggal_produksi','car_id'];
    public $incrementing = false;

    public function manufactures()
    {
        return $this->belongsTo('App\Manufactures');
    }
}
