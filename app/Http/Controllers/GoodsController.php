<?php

namespace App\Http\Controllers;

use App\Goods;
use App\Car;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\GoodsImage;
use Illuminate\Support\Str;

class GoodsController extends Controller
{

    public function __construct()
    {
        $this->goods = new Goods();
        $this->image = new GoodsImage();
		$this->cars = new Car();
        //$this->image = new GoodsImage();
    }

    public function index()
    {

        return view('backend.goods.index');
    }

    public function source(){
        $query= Goods::query();
        $query->with(['manufactures']);
        return DataTables::eloquent($query)
        ->filter(function ($query) {
            if (request()->has('search')) {
                $query->where(function ($q) {
                    $q->where('name', 'LIKE', '%' . request('search')['value'] . '%');
                });
            }
            })
            ->addColumn('name', function ($data) {
                return str_limit($data->name,50);
            })
            ->addColumn('manufacture_id', function ($data) {
                return title_case($data->manufactures->name);
            })
            ->addColumn('detail', function ($data) {
                return $data->detail;
            })
            ->addColumn('weight', function ($data) {
                return $data->weight;
            })
			
            ->addColumn('weight_stock', function ($data) {
                return $data->weight_stock;
            })
			
            ->addColumn('jumlah_shipment', function ($data) {
                return $data->jumlah_shipment;
            })
            ->addColumn('tanggal_produksi', function ($data) {
                return $data->tanggal_produksi;
            })
            ->addColumn('price', function ($data) {
                return number_format($data->price,0,',','.');
            })
           
			 ->addColumn('car_id', function ($data) {
                 return $data->car_id;
            })
            ->addColumn('status', function ($data) {
                return $data->status == 'tersedia' ? '<span class="badge badge-success">'.$data->status.'</span>':'<span class="badge badge-secondary">'.$data->status.'</span>';
            })
            ->addColumn('description', function ($data) {
                return str_limit(strip_tags($data->description,50));
            })
            ->addIndexColumn()
            ->addColumn('action', 'backend.goods.index-action')
            ->rawColumns(['action','status'])
            ->toJson();
    }

    public function create()
    {
		$items = Car::all(['id', 'name']);

        return view('backend.goods.create', compact('items',$items));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request = $request->merge(['slug'=> str_slug($request->name),'status'=>'tersedia']);
            $goods = $this->goods->create($request->all());
            $no=1;
            foreach($request->image as $row){
                $fileName = Str::uuid();
                $file = $row->storeAs(
                    'public/image/goods',$fileName.'.'.$row->extension()
                );
                $this->image->create([
                    'barang_id'=>$goods->id,
                    'image'=>'storage/image/goods/'.$fileName.'.'.$row->extension()
                ]);
            }
            DB::commit();
            return redirect()->route('goods.index')->with('success-message','Data telah disimpan');
        } catch (\exception $e) {
            DB::rollback();
            return redirect()->route('goods.index')->with('error-message',$e->getMessage());
        }


    }

    public function show($id)
    {
        $data = $this->goods->find($id);
        return $data;

    }

    public function edit($id)
    {
        $data = $this->goods->find($id);
        return view('backend.goods.edit',compact('data'));

    }

    public function update(Request $request, $id)
    {
        $request = $request->merge(['slug'=>str_slug($request->name)]);
        if($request->has('image')){
            foreach($request->image as $row){
                $fileName = Str::uuid();
                $file = $row->storeAs(
                    'public/image/goods',$fileName.'.'.$row->extension()
                );
                $this->image->create([
                    'barang_id'=>$id,
                    'image'=>'storage/image/goods/'.$fileName.'.'.$row->extension()
                ]);
            }
        }
        $this->goods->find($id)->update($request->all());
        return redirect()->route('goods.index')->with('success-message','Data telah dirubah');
    }

    public function destroy($id)
    {
         $this->goods->destroy($id);
         return redirect()->route('goods.index')->with('success-message','Data telah dihapus');

    }

    public function getImage($id){
        return $this->image->where('barang_id',$id)->get();
    }

    public function destroyImage($id){
        $this->image->destroy($id);
    }


}
